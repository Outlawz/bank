package mg.bank.services;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import mg.bank.enumeration.OperationType;
import mg.bank.model.Account;
import mg.bank.model.Operation;
import mg.bank.repository.AccountRepository;
import mg.bank.repository.OperationRepository;

@Service
public class AccountService {

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private OperationRepository operationRepository;

	public List<Account> findAll() {
		List<Account> all = new ArrayList<Account>();
		Iterable<Account> iterator = this.accountRepository.findAll();
		iterator.forEach(all::add);
		return all;
	}

	public List<Operation> operations(long id) {
		List<Operation> operations = new ArrayList<Operation>();
		for (Operation o : operationRepository.findAll()) {
			if (o.getMyAccount().getId() == id) {
				operations.add(o);
			}
		}
		operations.sort(Comparator.comparing(Operation::getId).reversed());
		return operations;
	}

	public Account getLoggedUser() {
		String username = "";
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken))
			username = auth.getName();
		return findByUserName(username);
	}

	public Account findByUserName(String userName) {
		Account account = null;
		for (Account a : this.findAll()) {
			if (a.getUserName().equalsIgnoreCase(userName)) {
				account = a;
				break;
			}
		}
		return account;
	}

	public Account findByAccountNumber(long id) {
		Account account = null;
		for (Account a : this.findAll()) {
			if (a.getAccountNumber() == id) {
				account = a;
				break;
			}
		}
		return account;
	}

	/**
	 * CREDIT
	 * 
	 * @param account
	 * @param cost
	 */
	public void credit(Account account, long cost, Account by) {
		account.setBalance(account.getBalance() + cost);
		accountRepository.save(account);

		Date d = new Date();
		Operation operation = new Operation();
		operation.setCreatedAt(d);
		operation.setAmount(cost);
		operation.setBy(by.getFirstName() + ' ' + by.getName());
		operation.setOperationNumber(d.getTime() + operation.getId());
		operation.setAmount(cost);
		operation.setAccount(account);
		operation.setOperation(OperationType.CREDIT);
		operationRepository.save(operation);
	}

	/**
	 * DEBIT
	 * 
	 * @param account
	 * @param cost
	 */
	public void debit(Account account, long cost, Account by) {
		account.setBalance(account.getBalance() - cost);
		accountRepository.save(account);

		Date d = new Date();
		Operation operation = new Operation();
		operation.setCreatedAt(d);
		operation.setAmount(cost);
		operation.setBy(by.getFirstName() + ' ' + by.getName());
		operation.setOperationNumber(d.getTime() + operation.getId());
		operation.setAmount(cost);
		operation.setAccount(account);
		operation.setOperation(OperationType.DEBIT);
		operationRepository.save(operation);
	}
}
