package mg.bank.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mg.bank.model.Operation;
import mg.bank.repository.OperationRepository;

@Service
public class OperationService {

	@Autowired
	private OperationRepository operationRepository;

	public List<Operation> findAll() {
		List<Operation> all = new ArrayList<Operation>();
		Iterable<Operation> iterator = this.operationRepository.findAll();
		iterator.forEach(all::add);
		return all;
	}

	public Operation findById(long id) {
		Operation Operation = null;
		for (Operation a : this.findAll()) {
			if (a.getId() == id) {
				Operation = a;
				break;
			}
		}
		return Operation;
	}
}
