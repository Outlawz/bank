package mg.bank.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import mg.bank.enumeration.OperationType;

@Entity
@Table(name = "operations")
@JacksonXmlRootElement(localName = "Operation")
public class Operation implements Serializable {

	@Id
	@GeneratedValue
	@JacksonXmlProperty(isAttribute = true)
	private long id;

	@NotNull
	@JacksonXmlProperty
	private long operationNumber;

	@NotNull
	@JacksonXmlProperty
	private long amount;

	@NotNull
	@JacksonXmlProperty
	private String by;

	@JsonFormat(pattern = "yyyy-MM-dd")
	@NotNull
	@Column(name = "operation_type")
	@JacksonXmlProperty
	private OperationType operation;

	@NotNull
	@JacksonXmlProperty
	private Date createdAt;

	@JsonFormat(pattern = "yyyy-MM-dd")
	@JacksonXmlProperty
	private Date updatedAt;

	@JsonFormat(pattern = "yyyy-MM-dd")
	@JacksonXmlProperty
	private Date deletedAt;

	@ManyToOne
	@JoinColumn(name = "account_id", referencedColumnName = "id")
	@JacksonXmlProperty
	@JacksonXmlElementWrapper(useWrapping = true)
	private Account myAccount;

	public Operation() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getOperationNumber() {
		return operationNumber;
	}

	public void setOperationNumber(long operationNumber) {
		this.operationNumber = operationNumber;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public OperationType getOperation() {
		return operation;
	}

	public void setOperation(OperationType operation) {
		this.operation = operation;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Account getAccount() {
		return myAccount;
	}

	public void setAccount(Account account) {
		this.myAccount = account;
	}

	public String getBy() {
		return by;
	}

	public void setBy(String by) {
		this.by = by;
	}

	public Account getMyAccount() {
		return myAccount;
	}

	public void setMyAccount(Account myAccount) {
		this.myAccount = myAccount;
	}
}
