package mg.bank.enumeration;

public enum OperationType {
	CREDIT, DEBIT;
}
