package mg.bank.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mg.bank.model.Operation;
import mg.bank.services.OperationService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/operation")
public class OperationController {

	@Autowired
	private OperationService operationService;

	@GetMapping(value = "/all", produces = MediaType.APPLICATION_XML_VALUE)
	public List<Operation> findAll() {
		return this.operationService.findAll();
	}
}
