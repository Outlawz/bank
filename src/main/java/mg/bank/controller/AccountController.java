package mg.bank.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mg.bank.model.Account;
import mg.bank.model.Operation;
import mg.bank.repository.AccountRepository;
import mg.bank.services.AccountService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/account")
public class AccountController {

	@Autowired
	private AccountService accountService;

	@Autowired
	private AccountRepository accountRepository;

	@GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Account> findAll() {
		return this.accountService.findAll();
	}

	@GetMapping(value = "/logged", produces = MediaType.APPLICATION_JSON_VALUE)
	public Account getLoggedUser() {
		return this.accountService.getLoggedUser();
	}

	@PostMapping(value = "/credit/{amount}/{accountId}/{byId}", produces = MediaType.APPLICATION_XML_VALUE)
	public Account crediter(@PathVariable long amount, @PathVariable long accountId, @PathVariable long byId) {
		Account account = accountService.findByAccountNumber(accountId);
		Account by = accountService.findByAccountNumber(byId);
		accountService.credit(account, amount, by);
		accountRepository.save(account);
		return account;
	}

	@PostMapping(value = "/debit/{amount}/{accountId}/{byId}", produces = MediaType.APPLICATION_XML_VALUE)
	public Account debiter(@PathVariable long amount, @PathVariable long accountId, @PathVariable long byId) {
		Account account = accountService.findByAccountNumber(accountId);
		Account by = accountService.findByAccountNumber(byId);
		accountService.debit(account, amount, by);
		accountRepository.save(account);
		return account;
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Account getAccount(@PathVariable long id) {
		return this.accountService.findByAccountNumber(id);
	}

	@GetMapping(value = "/operations", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Operation> getOperations() {
		return this.accountService.operations(this.accountService.getLoggedUser().getId());
	}
}
